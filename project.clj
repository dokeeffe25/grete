(defproject grete "0.1.0-SNAPSHOT"
  :description "Clojure wrapper for the Java Kafka Client"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.apache.kafka/kafka-clients "0.10.2.1"]
                 [prismatic/schema "1.1.5"]])
